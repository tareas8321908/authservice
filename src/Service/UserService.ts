import { ref } from 'vue'
import type { Ref } from 'vue'
import type { IUser } from '@/interface/IUser' 

const url = import.meta.env.VITE_API_URL || "https://utcancun.a.pinggy.online" // "http://172.16.107.120:3000"

export default class UserService{
    private User: Ref<IUser>
    private Users: Ref<IUser[]>

    constructor(){
        this.User = ref({}) as Ref<IUser>
        this.Users = ref<Array<IUser>>([])
    }

    getUsers():Ref<IUser[]> {
        return this.Users
        
    }

    getUser():Ref<IUser> {
        return this.User
        
    }

    async ObtenerTodo(): Promise<void>  {
        try{
            const response = await fetch (url + '/Users')
            const json = await response.json()
            this.Users.value = await json

        } catch(error){
            console.log(error)
        }
    }

    async ObtenerUser(email: string): Promise<void> {
        try {
          const json = await fetch(url + '/User' + email)
          const response = await json.json()
          this.User.value = await response
        } catch (error) {
          console.log(error)
        }
      }

    async CrearUser( email: string, password: string, name: string, group: string): Promise<void> {
        try {
          const response = await fetch(url + '/Register', {
            method: 'POST',
            headers: {
              'Content-Type': 'application/json'
            },
            body: JSON.stringify({"email": email,"password": password, "name": name,"group": group })
        })
    
    const responseData = await response.json()
    console.log(responseData)

    } catch (error) {
        console.log(error)
        }
      }
}